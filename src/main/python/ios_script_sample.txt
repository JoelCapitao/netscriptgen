

Imbriquation de requêtes
{{VLAN!{{Global!HOST1:VLAN#Admin}}:Name}}

Récupérer un bloc de script
[[Feuille!Default]] ou [[Feuille]] --> que le bloc par défaut
[[Feuille!Nom_du_bloc_du_script]] --> on spécifie le bloc qu'on veut
[[Feuille!all]] -> tous les blocs de script



hostname {{hostname}}

! Vlan configuration

vlan {{VLAN#Admin}}-11
 name {VLAN![{$VLAN#Admin}, Name]}-EDBG-ADMRESEAU-1

vlan {{VLAN#Data}}-101
 name {VLAN![{$VLAN#Data}, Name]}-EDBG-DATA-01

vlan {{VLAN#Voice}}-601
 name {VLAN![{$VLAN#Voice}, Name]}-EDBG-VOIX-01

vlan {{VLAN#Native}}-901
 name {VLAN![{$VLAN#Native}, Name]}-EDBG-NATIF-01

vlan {{VLAN#Wifi}}-21
 name {VLAN![{$VLAN#Wifi}, Name]}-EDBG-ADMWIFI-1

ip dhcp snooping vlan {{VLAN#Wifi}}-21,{{VLAN#Data}}-101,{{VLAN#Voice}}-601
no ip dhcp snooping information option
ip dhcp snooping database flash:dhcp-snooping.txt
ip dhcp snooping database write-delay 60
ip dhcp snooping
ip device tracking
ip arp inspection vlan {{VLAN#Wifi}}-21,{{VLAN#Data}}-101,{{VLAN#Voice}}-601
ip arp inspection log-buffer entries 1024
ip arp inspection log-buffer logs 10 interval 1

arp access-list FIXED-IP-MAC
permit ip {VLAN![{$VLAN#Data}, Subnet_arp]}-10.179.1.224 {VLAN![{$VLAN#Data}, Wildcard_arp]}-0.0.0.15 mac any

ip arp inspection filter FIXED-IP-MAC vlan {{VLAN#Data}}-101
--------------------------

! Configuration Basée sur le template telindus v0.9
!
! Configuration Globale
!
service nagle
no service pad
service tcp-keepalives-in
service tcp-keepalives-out
service timestamps debug datetime msec localtime show-timezone
service timestamps log datetime msec localtime show-timezone
service password-encryption
service sequence-numbers
service counters max age 5
no service dhcp

hostname {{hostname}}


logging file flash:switch-log.txt 64000 1024 debugging
logging buffered 64000 debugging
logging console critical

enable secret level 14 {{password}}
enable secret {{password}}

username ultime privilege 15 secret {{password}}
username telindus privilege 15 secret {{password}}

aaa new-model

aaa authentication login CONSOLE none
aaa authorization exec CONSOLE none
aaa authorization console

aaa authentication login LOCALE local
aaa authorization exec LOCALE local

aaa authentication login RADIUS-EDF group radius local enable
aaa authorization exec RADIUS-EDF group radius local
aaa accounting exec RADIUS-EDF start-stop group radius

aaa session-id common
clock timezone GMT+1 1
clock summer-time GMT+1 recurring last Sun Mar 2:00 last Sun Oct 3:00

switch 1 priority 15

4 switch 2 priority 14

4 switch 3 priority 13

4 switch 4 priority 12

4 switch 5 priority 11

4 switch 6 priority 10

4 switch 7 priority 9

4 switch 8 priority 8

4 switch 9 priority 7


stack-mac persistent timer 0

system mtu routing 1500


{{vtp}}
vtp version 2
vtp domain RLE-DBG-1
vtp password RL3-DBG-1$
vtp mode transparent

udld enable

no ip source-route
no ip gratuitous-arps
ip options drop

{{dns}}
ip domain-lookup
ip domain-name sira.dept.edf.fr
ip name-server 192.196.111.47
ip name-server 130.98.194.189

crypto key generate rsa general-keys modulus 1024


ip ssh time-out 30
ip ssh authentication-retries 2
ip ssh version 2

{{vlan}}
vlan 11
 name EDBG-ADMRESEAU-1

vlan 101
 name EDBG-DATA-01

vlan 601
 name EDBG-VOIX-01

vlan 901
 name EDBG-NATIF-01

vlan 21
 name EDBG-ADMWIFI-1

ip dhcp snooping vlan 21,101,601
no ip dhcp snooping information option
ip dhcp snooping database flash:dhcp-snooping.txt
ip dhcp snooping database write-delay 60
ip dhcp snooping
ip device tracking
ip arp inspection vlan 21,101,601
ip arp inspection log-buffer entries 1024
ip arp inspection log-buffer logs 10 interval 1

arp access-list FIXED-IP-MAC
permit ip 10.179.1.224 0.0.0.15 mac any

ip arp inspection filter FIXED-IP-MAC vlan 101

spanning-tree mode rapid-pvst
spanning-tree loopguard default
spanning-tree portfast default
spanning-tree portfast bpduguard default
spanning-tree etherchannel guard misconfig
spanning-tree extend system-id

errdisable recovery cause bpduguard
errdisable recovery cause channel-misconfig
errdisable recovery cause udld
errdisable recovery cause psecure-violation
errdisable recovery cause dhcp-rate-limit
errdisable recovery cause arp-inspection

ip subnet-zero

! Configuration QOS
!
mls qos

mls qos map policed-dscp  0 16 18 34 40 to 8

mls qos map dscp-cos  0 1 2 3 4 5 6 7 to 0
mls qos map dscp-cos  8 9 10 11 12 13 14 15  to 0
mls qos map dscp-cos  16 17 18 19 20 21 22 23 to 0
mls qos map dscp-cos  24 25 26 27 28 29 30 31 to 0
mls qos map dscp-cos  32 33 34 35 36 37 38 39 to 0
mls qos map dscp-cos  40 41 42 43 44 45 46 47 to 0
mls qos map dscp-cos  0 to 0
mls qos map dscp-cos  8 to 1
mls qos map dscp-cos  16 to 2
mls qos map dscp-cos  18 to 2
mls qos map dscp-cos  34 to 4
mls qos map dscp-cos  40 to 3
mls qos map dscp-cos  46 to 5
mls qos map dscp-cos  48 to 6
mls qos map dscp-cos  56 to 7

mls qos map cos-dscp 0 8 16 24 32 46 48 56

mls qos srr-queue output cos-map queue 1 threshold 3 5
mls qos srr-queue output cos-map queue 2 threshold 1 2 4
mls qos srr-queue output cos-map queue 2 threshold 2 3
mls qos srr-queue output cos-map queue 2 threshold 3 6 7
mls qos srr-queue output cos-map queue 3 threshold 3 0
mls qos srr-queue output cos-map queue 4 threshold 1 1

mls qos srr-queue output dscp-map queue 1 threshold 3 46
mls qos srr-queue output dscp-map queue 2 threshold 1 16 18 34
mls qos srr-queue output dscp-map queue 2 threshold 2 40
mls qos srr-queue output dscp-map queue 2 threshold 3 48 56
mls qos srr-queue output dscp-map queue 3 threshold 3 0
mls qos srr-queue output dscp-map queue 4 threshold 1 8

mls qos queue-set output 1 buffers 15 30 35 20

mls qos queue-set output 1 threshold 1 100 100 100 100
mls qos queue-set output 1 threshold 2 1200 1400 50 1600
mls qos queue-set output 1 threshold 3 100 100 50 800
mls qos queue-set output 1 threshold 4 60 100 50 800

lldp run

network-policy profile 1
 voice vlan {{voice_vlan}}601 cos 5
 voice vlan {{voice_vlan}}601 dscp 46
 voice-signaling vlan {{voice_vlan}}601 cos 3
 voice-signaling vlan {{voice_vlan}}601 dscp 40
exit

port-channel load-balance src-dst-ip

ip access-list extended CRITIQUE

ip access-list extended DATA
permit ip 10.179.1.0 0.0.0.255 any

ip access-list extended MGMT
 permit ip 10.179.255.0 0.0.0.255 any
 permit ip any 10.179.255.0 0.0.0.255
 permit ip 10.179.0.0 0.0.0.255 any
 permit ip any 10.179.0.0 0.0.0.255

ip access-list extended TOIP-ANY
permit ip 10.74.97.0 0.0.0.255 any

ip access-list extended TOIP-SIGNALISATION
permit udp 10.74.97.0 0.0.0.255 any dscp cs3
permit udp 10.74.97.0 0.0.0.255 any dscp cs5

ip access-list extended TOIP-VOIX
permit udp 10.74.97.0 0.0.0.255 any dscp ef

ip access-list extended VIDEO
!

class-map match-all CRITIQUE
 match access-group name CRITIQUE

class-map match-all DATA
 match access-group name DATA

class-map match-all MGMT
 match access-group name MGMT

class-map match-all TOIP-VOIX
 match access-group name TOIP-VOIX

class-map match-all VIDEO
 match access-group name VIDEO


class-map match-all TOIP-SIGNALISATION
 match access-group name TOIP-SIGNALISATION

class-map match-all TOIP-ANY
 match access-group name TOIP-ANY


policy-map QoS

class TOIP-VOIX
  set ip dscp ef
  police 128000 8000 exceed-action drop

 class TOIP-SIGNALISATION
  set ip dscp cs5
  police 32000 8000 exceed-action policed-dscp-transmit

 class TOIP-ANY
  set ip dscp 0
  police 32000 8000 exceed-action policed-dscp-transmit

 class VIDEO
  set ip dscp 34
  police 15000000 256000 exceed-action policed-dscp-transmit

 class MGMT
  set ip dscp 16
  police 1000000 8000 exceed-action policed-dscp-transmit

 class DATA
  set ip dscp 0
  police 5000000 8000 exceed-action policed-dscp-transmit

 class CRITIQUE
  set ip dscp 18
  police 5000000 8000 exceed-action policed-dscp-transmit

 class class-default
  set ip dscp default
  police 5000000 8000 exceed-action policed-dscp-transmit

! Configuration ports FO vers Cœurs

!
interface range Giga 1/0/1 , Giga 2/0/1
 description EDBG-R04-01
 switchport trunk encapsulation dot1q
 switchport trunk native vlan 901
 switchport trunk allowed vlan 11,21,101,601,901
 switchport mode trunk
 switchport nonegotiate
 srr-queue bandwidth share 1 70 25 5
 priority-queue out
 mls qos trust dscp
 ip arp inspection trust
 ip dhcp snooping trust
 channel-group 1 mode active
 no shut
exit

interface Port-channel1
 description EDBG-R04-01
 switchport trunk encapsulation dot1q
 switchport trunk native vlan 901
 switchport trunk allowed vlan 11,21,101,601,901
 switchport mode trunk
 switchport nonegotiate
 ip arp inspection trust
 ip dhcp snooping trust
 no shut
exit

! Configration ports utilisateurs
!
interface range FastEthernet1/0/1-44 4 4 4 4 4 4 4 4
 switchport access vlan 101
 switchport mode access
 switchport port-security maximum 3
 switchport port-security
 switchport port-security aging time 2
 switchport port-security violation restrict
 switchport port-security aging type inactivity
 ip device tracking maximum 10
 no snmp trap link-status
 storm-control broadcast level 1.00 0.50
 storm-control action trap
 no cdp enable
 spanning-tree portfast
 spanning-tree bpduguard enable
 service-policy input QoS
 srr-queue bandwidth share 1 70 25 5
 priority-queue out
 lldp transmit
 lldp receive
 lldp med-tlv-select network-policy
 ip verify source
 ip dhcp snooping limit rate 10
 network-policy 1

interface range FastEthernet1/0/45-48 4 4 4 4 4 4 4 4
 description Borne WIFI
 switchport access vlan 21
 switchport mode access
 switchport protected
 switchport port-security
 switchport port-security maximum 1
 switchport port-security aging time 2
 switchport port-security violation restrict
 switchport port-security aging type inactivity
 ip device tracking maximum 10
 srr-queue bandwidth share 1 70 25 5
 priority-queue out
 no snmp trap link-status
 storm-control broadcast level 1.00 0.50
 storm-control action trap
 no cdp enable
 no cdp tlv server-location
 no cdp tlv app
 spanning-tree portfast
 spanning-tree bpduguard enable
 ip verify source
 ip dhcp snooping limit rate 10
shut

! Configuration Management
interface Vlan1
 no ip address
 shutdown

interface Vlan 11
 description EDBG-ADMRESEAU-1
 ip address 10.179.255.1 255.255.255.192
 no shut
!
ip default-gateway 10.179.255.62
ip classless
no ip http server
no ip http secure-server
!


ip access-list standard SUP-ACCESS-RO
remark Liste des postes autorises en lecture en SNMP (RO)
 remark Serveur Supervision What S UP
permit 10.122.47.65
!
ip access-list standard SUP-ACCESS-RW
 remark Liste des postes autorises en ecriture en SNMP (RW)
 remark Serveur Administration LMS
 permit 10.181.225.10





logging trap debugging
logging facility local2
logging 10.181.225.10

snmp-server group SUPERVISION-RO v3 auth read sup-view access SUP-ACCESS-RO
snmp-server group SUPERVISION-RW v3 auth write sup-view access SUP-ACCESS-RW

snmp-server group SUPERVISION-RW v3 auth context vlan-101
snmp-server group SUPERVISION-RW v3 auth context vlan-601
snmp-server group SUPERVISION-RO v3 auth context vlan-101
snmp-server group SUPERVISION-RO v3 auth context vlan-601

snmp-server user sup-reseau SUPERVISION-RO v3 auth md5 oi6p72 priv aes 128 oi6p72

snmp-server user lms-3LY SUPERVISION-RW v3 auth md5 RL3-3LY-LMS-00 priv aes 128 RL3-3LY-LMS-99

snmp-server view sup-view internet included
snmp-server queue-length 50
snmp-server location 01_BUGEY_BAT38_1er_LT04_38B142LTT060AR
snmp-server contact N1_08.10.00.67.02
snmp-server system-shutdown
snmp-server enable traps snmp authentication linkdown linkup coldstart warmstart
snmp-server enable traps cpu threshold
snmp-server enable traps port-security trap-rate 1
snmp-server enable traps envmon fan shutdown supply temperature status
snmp-server enable traps stackwise
snmp-server enable traps storm-control trap-rate 1
snmp-server enable traps bridge newroot topologychange
snmp-server host 10.122.47.65 version 3 auth sup-reseau
snmp ifmib ifindex persist

banner motd ^
*************************************************************
*                                                           *
* ATTENTION ! Cet equipement est la propriete de EDF.       *
* Tout acces non autorise est formellement interdit.        *
* Toute personne utilisant cette ressource sans  y etre     *
* autorisee est passible de poursuites judiciaires.         *
*                                                           *
* BE CAREFUL ! This equipment is EDF property ...           *
* Unauthorized acces is forbidden.                          *
* Unauthorized use of this facility is prohibited.          *
*                                                           *
*************************************************************
^

alias exec ps show proc cpu | e 0.00%__0.00%__0.00%

privilege exec level 14 traceroute
privilege exec level 14 ping
privilege exec level 14 show tech-support
privilege exec level 14 show dhcp
privilege exec level 14 show standby
privilege exec level 14 show startup-config
privilege exec level 14 show access-lists
privilege exec level 14 show logging

line con 0
 privilege level 0
 logging synchronous
 login authentication LOCALE
 authorization exec LOCALE

line vty 0 15
 logging synchronous
 login authentication LOCALE
 authorization exec LOCALE
 transport input ssh

!
ntp authentication-key 2 md5 RL3-DBG-NTP
ntp authenticate
ntp trusted-key 2
ntp source vlan 11
ntp server 10.179.0.2 key 2 prefer
ntp server 10.179.0.1 key 2
end