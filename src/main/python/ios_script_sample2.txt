
Récupérer la valeur d'un paramètre dans un tableau
{{arrayparsing_test!21:Subnet}}

Mon hostname est : {{hostname}}
Mon type de switch est : {{type}}

{{VTP_Profile}}

Récupérer un paramètre d'une liste
{{listparsing_test!DNS:DNS Server:2}}

Récupérer un texte
{{textparsing_test!banner}}

Imbriquation de requêtes
{{VLAN!((Global!((Global variable!hostname:host:1)):VLAN#Admin)):Name}}



